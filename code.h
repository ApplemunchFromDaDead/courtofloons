void caseStep() {
	switch (move) {
		case 0:
			changeDialogue(CHAR_LUKIFER, 0, 0, "...");
			break;
		case 1:
			signalMisc(COL_MISC_NOCHANGE);
			playSound(1);
			changeDialogue(CHAR_LUKIFER, 3, 0, "what were we doing here again?");
			break;
		case 2:
			changeDialogue(CHAR_REDMOND, 0, 1, "You seriously forgot?");
			break;
		case 3:
			changeDialogue(CHAR_REDMOND, 6, 1, "We're here for the case of...");
			break;
		case 4:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_REDMOND, 0, 1, "");
			break;
		case 5:
			changeDialogue(CHAR_REDMOND, 3, 1, "Actually, what ARE we doing here?");
			break;
		case 6:
			changeDialogue(CHAR_GANDREY, 0, 3, "This is the court case for Harold Cranker, accused for the murder of Richie Buttons.");
			break;
		case 7:
			changeDialogue(CHAR_HAROLD, 0, 2, "Say hi to the Judge, everybody!");
			break;
		case 8:
			changeDialogue(CHAR_REDMOND, 2, 1, "Dude, we're the ones lawyering your ass, because you happened to COMMIT A MURDER!");
			break;
		case 9:
			changeDialogue(CHAR_LUKIFER, 1, 0, "I don't really know Harold as the kind of guy who'd commit a murder.");
			break;
		case 10:
			changeDialogue(CHAR_LUKIFER, 0, 0, "Honestly I always believed that, if he went to court, he'd be in there for,");
			break;
		case 11:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_LUKIFER, 6, 0, "I dunno, ");
			break;
		case 12:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_LUKIFER, 0, 0, "stealing a feral rabbit or something odd like that.");
			break;
		case 13:
			changeDialogue(CHAR_LUKIFER, 3, 0, "I keep wondering if that's even illegal...");
			break;
		case 14:
			changeDialogue(CHAR_GANDREY, 0, 3, "Enough chatter! Court is now in session for the trial of Harold Cranker.");
			break;
		case 15:
			changeDialogue(CHAR_GANDREY, 0, 3, "Prosecution, if you may give your opening statement.");
			break;
		case 16:
			changeDialogue(CHAR_REDMOND, 0, 1, "Yes, Your Honor.");
			break;
		case 17:
			changeDialogue(CHAR_REDMOND, 1, 1, "Harold was seen at an amusement park, via security cameras, that displayed him assaulting and murdering Richie at the scene.");
			break;
		case 18:
			changeDialogue(CHAR_REDMOND, 0, 1, "Detectives at the crime scene saw no blood whatsoever, nor any form of bodily injury. The autopsy report was not handed in as of yet, but we have professionals dissecting the corpse as we speak.");
			break;
		case 19:
			changeDialogue(CHAR_REDMOND, 7, 1, "What detectives DID see of the deceased, however, was odd marks and string-like fibres that resemble a stuffed plush.");
			break;
		case 20:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_LUKIFER, 5, 0, "The body also seemed to be hollow, and having a cotton-like substance foaming from both its mouth and a large tear in the dermis.");
			break;
		case 21:
			changeDialogue(CHAR_REDMOND, 0, 1, "Judge, if you may, examine these two pieces extracted from the scene for evidence.");
			break;
		case 22:
						changeDialogue(CHAR_GANDREY, 2, 3, "[KNIFE and SECURITY TAPE added to Evidence]");
			break;
	}
};
