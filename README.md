# Court of Loons

Court of Loons is an adventure game / funny courtroom shenanigan creation engine inspired by Objection.lol and Ace Attorney. The game is programmed sucklessly with scripts to turn human readable, formatted plaintext into C code to compile into the game.

**NOTE: CoL currently uses Freedoom music as placeholders for unfinished trachs! They are licensed under the BSD 3-Clause license.**

# TO-DO

- Make an SDL2 frontend. It should not only look and feel similar to an Ace Attorney game, at least to Objection.lol, but also be the basis for other graphical frontends
