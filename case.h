void caseStep() {
	switch (move) {
		case 0:
			playMusic(0);
			changeDialogue(CHAR_LUKIFER, 0, 0, "...");
			break;
		case 1:
			signalMisc(COL_MISC_NOCHANGE);
			playSound(1);
			changeDialogue(CHAR_LUKIFER, 3, 0, "what were we doing here again?");
			break;
		case 2:
			changeDialogue(CHAR_REDMOND, 0, 1, "You seriously forgot?");
			break;
		case 3:
			changeDialogue(CHAR_REDMOND, 6, 1, "We\'re here for the case of...");
			break;
		case 4:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_REDMOND, 0, 1, "");
			break;
		case 5:
			changeDialogue(CHAR_REDMOND, 3, 1, "Actually, what ARE we doing here?");
			break;
		case 6:
			changeDialogue(CHAR_GANDREY, 0, 3, "This is the court case for Harold Cranker, accused for the murder of Richie Buttons.");
			break;
		case 7:
			changeDialogue(CHAR_HAROLD, 0, 2, "Say hi to the Judge, everybody!");
			break;
		case 8:
			changeDialogue(CHAR_REDMOND, 2, 1, "Dude, we\'re the ones lawyering your ass, because you happened to COMMIT A MURDER!");
			break;
		case 9:
			changeDialogue(CHAR_LUKIFER, 1, 0, "I don\'t really know Harold as the kind of guy who\'d commit a murder.");
			break;
		case 10:
			changeDialogue(CHAR_LUKIFER, 0, 0, "Honestly I always believed that, if he went to court, he\'d be in there for,");
			break;
		case 11:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_LUKIFER, 6, 0, "I dunno, ");
			break;
		case 12:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_LUKIFER, 0, 0, "stealing a feral rabbit or something odd like that.");
			break;
		case 13:
			changeDialogue(CHAR_LUKIFER, 3, 0, "I keep wondering if that\'s even illegal...");
			break;
		case 14:
			changeDialogue(CHAR_GANDREY, 0, 3, "Enough chatter! Court is now in session for the trial of Harold Cranker.");
			break;
		case 15:
			changeDialogue(CHAR_GANDREY, 0, 3, "Prosecution, if you may give your opening statement.");
			break;
		case 16:
			changeDialogue(CHAR_REDMOND, 0, 1, "Yes, Your Honor.");
			break;
		case 17:
			playMusic(1);
			changeDialogue(CHAR_REDMOND, 1, 1, "Harold was seen at an amusement park, via security cameras, that displayed him assaulting and murdering Richie at the scene.");
			break;
		case 18:
			changeDialogue(CHAR_REDMOND, 0, 1, "Detectives at the crime scene saw no blood whatsoever, nor any form of bodily injury. The autopsy report was not handed in as of yet, but we have professionals dissecting the corpse as we speak.");
			break;
		case 19:
			changeDialogue(CHAR_REDMOND, 7, 1, "What detectives DID see of the deceased, however, was odd marks and string-like fibres that resemble a stuffed plush.");
			break;
		case 20:
			signalMisc(COL_MISC_NOCHANGE);
						changeDialogue(CHAR_LUKIFER, 4, 0, "The body also seemed to be hollow, and having a cotton-like substance foaming from both its mouth and a large tear in the dermis.");
			break;
		case 21:
			changeDialogue(CHAR_REDMOND, 0, 1, "Judge, if you may, examine these two pieces extracted from the scene for evidence.");
			break;
		case 22:
						playSound(5);
			changeDialogue(CHAR_GANDREY, 2, 3, "[KNIFE and SECURITY TAPE added to Evidence]");
			break;
		case 23:
						changeDialogue(CHAR_LUKIFER, 3, 0, "What a disaster of proof, this seems-");
			break;
		case 24:
			stopMusic();
			changeDialogue(CHAR_LUKIFER, 0, 0, "Wait a minute, let us have a look at that tape.");
			break;
		case 25:
						changeDialogue(CHAR_REDMOND, 1, 1, "...");
			break;
		case 26:
						playSound(2);
			playMusic(2);
			changeDialogue(CHAR_REDMOND, 4, 1, "!");
			break;
		case 27:
			changeDialogue(CHAR_LUKIFER, 0, 0, "While Harold may have ripped Richie from that spot...");
			break;
		case 28:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_LUKIFER, 6, 0, "I highly doubt that is \"Richie\".");
			break;
		case 29:
			changeDialogue(CHAR_LUKIFER, 1, 0, "As you can see clearly in the footage, when Harold snags her,");
			break;
		case 30:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_LUKIFER, 0, 0, "she leaves hehind a robotic skeleton. ");
			break;
		case 31:
			changeDialogue(CHAR_LUKIFER, 6, 0, "Richie is, assumedly, a living being, so why would she have the skeleton of an animatronic?");
			break;
		case 32:
			playSound(0);
			changeDialogue(CHAR_REDMOND, 2, 1, "Nonsense! ");
			break;
		case 33:
			changeDialogue(CHAR_REDMOND, 6, 1, "Oh, the autopsy report has just arrived, too. Here, examine this.");
			break;
		case 34:
						playSound(5);
			changeDialogue(CHAR_GANDREY, 3, 0, "[AUTOPSY REPORT added to Evidence]");
			break;
		case 35:
			playMusic(1);
			changeDialogue(CHAR_REDMOND, 0, 1, "As detailed, she was seen to be deflated, as if she was hollow. The reason being her stuffing left her body, ");
			break;
		case 36:
			changeDialogue(CHAR_REDMOND, 0, 1, "and her skeleton was missing!");
			break;
		case 37:
			changeDialogue(CHAR_LUKIFER, 6, 0, "You missed one detail in that report.");
			break;
		case 38:
			changeDialogue(CHAR_LUKIFER, 3, 0, "Several, actually.");
			break;
		case 39:
			changeDialogue(CHAR_LUKIFER, 0, 0, "As it says, she had several fabric-like cuts across her body, stuffing was leaking out, and she was hollow.");
			break;
		case 40:
			changeDialogue(CHAR_LUKIFER, 3, 0, "Of course, this means Harold did an act of violence against another living being.");
			break;
		case 41:
			changeDialogue(CHAR_LUKIFER, 2, 0, "But I\'m surprised even YOU haven\'t found out that, just maybe,");
			break;
		case 42:
			signalMisc(COL_MISC_NOCHANGE);
			stopMusic();
			changeDialogue(CHAR_LUKIFER, 6, 0, "she wasn\'t even human!");
			break;
		case 43:
			playMusic(2);
			changeDialogue(CHAR_REDMOND, 2, 1, "You said ASSUMEDLY! Aren\'t lawyers supposed to use TRUTH rather than guesses?");
			break;
		case 44:
			changeDialogue(CHAR_LUKIFER, 1, 0, "Are either of us lawyers?");
			break;
		case 45:
			changeDialogue(CHAR_LUKIFER, 0, 0, "We\'re just some knuckleheads dragged from our own universes to have a faux-convincing trial.");
			break;
		case 46:
			stopMusic();
			changeDialogue(CHAR_LUKIFER, 5, 0, "...");
			break;
		case 47:
			changeDialogue(CHAR_LUKIFER, 6, 0, "Forget I said anything.");
			break;
		case 48:
			playMusic(0);
			changeDialogue(CHAR_REDMOND, 0, 1, "Regardless, the evidence speaks quite clearly.");
			break;
		case 49:
			changeDialogue(CHAR_REDMOND, 6, 1, "She is a stuffed animal.");
			break;
		case 50:
			changeDialogue(CHAR_LUKIFER, 2, 0, "NO.");
			break;
		case 51:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_LUKIFER, 2, 0, "SHE.");
			break;
		case 52:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_LUKIFER, 2, 0, "IS.");
			break;
		case 53:
			signalDraw(COL_DRAW_FLASH);
			signalDraw(COL_DRAW_SHAKE);
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_LUKIFER, 2, 0, "NOT!");
			break;
		case 54:
			changeDialogue(CHAR_LUKIFER, 6, 0, "If Richie wasn\'t even subject to human rights, by not being human, this entire trial would be pointless!");
			break;
		case 55:
			changeDialogue(CHAR_LUKIFER, 1, 0, "But look where we are.");
			break;
		case 56:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_LUKIFER, 3, 0, "Right in the clutches of the most insulting crime that doesn\'t even exist.");
			break;
		case 57:
			changeDialogue(CHAR_GANDREY, 0, 0, "I advise we return to the point.");
			break;
		case 58:
			changeDialogue(CHAR_LUKIFER, 0, 0, "Yes, Your Honor.");
			break;
		case 59:
			changeDialogue(CHAR_LUKIFER, 1, 0, "As Richie is obviously a human being, and knowing that we aren\'t filled with cotton and we bleed when cut, it\'s fair to say ");
			break;
		case 60:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_LUKIFER, 6, 0, "that\'s not her in the footage.");
			break;
		case 61:
			changeDialogue(CHAR_LUKIFER, 0, 0, "It is merely a costume attached to an animationic, filled with cotton to give it a plushy feeling.");
			break;
		case 62:
			changeDialogue(CHAR_REDMOND, 0, 0, "........");
			break;
		case 63:
			changeDialogue(CHAR_REDMOND, 6, 0, "That\'s actually coagulated blood.");
			break;
		case 64:
			changeDialogue(CHAR_LUKIFER, 7, 0, "WHA-");
			break;
		case 65:
			changeDialogue(CHAR_LUKIFER, 8, 0, "HOOOW!?");
			break;
		case 66:
			changeDialogue(CHAR_REDMOND, 1, 0, "We can argue that Richie may simply be a different species of humanoid creatures.");
			break;
		case 67:
			changeDialogue(CHAR_REDMOND, 6, 0, "While most may have iron blood like you and I, some may not.");
			break;
		case 68:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_LUKIFER, 8, 0, "In this case, her blood happens to be quite volumetric and, like how iron blood stiffens when exposed to air,");
			break;
		case 69:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_GANDREY, 5, 0, "her blood happens to coagulate near-immediately into this puffy, cotton-like form.");
			break;
		case 70:
			changeDialogue(CHAR_REDMOND, 0, 0, "As for the color of this blood, that can only be explained");
			break;
		case 71:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_REDMOND, 6, 0, "by semen.");
			break;
		case 72:
			signalMisc(COL_MISC_NOCHANGE);
			changeDialogue(CHAR_HAROLD, 4, 0, "You know how worms are gooey to the touch due to essentially being coated in reproductive fluids?");
			break;
		case 73:
			changeDialogue(CHAR_REDMOND, 6, 0, "This particular species of ours must use its blood as sperm.");
			break;
		case 74:
			changeDialogue(CHAR_LUKIFER, 8, 0, "...");
			break;
		case 75:
			changeDialogue(CHAR_LUKIFER, 5, 0, ".....");
			break;
		case 76:
			changeDialogue(CHAR_LUKIFER, 0, 0, "I am speechless.");
			break;
		case 77:
			changeDialogue(CHAR_LUKIFER, 6, 0, "However, it\'s clear you\'re talking out your ass.");
			break;
		case 78:
			changeDialogue(CHAR_LUKIFER, 2, 0, "You expect to tell a surgeon that semen is in your veins and have them believe you?");
			break;
		case 79:
			changeDialogue(CHAR_LUKIFER, 1, 0, "Shane to you.");
			break;
	}
};
