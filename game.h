// Game code for Court of Loons

#define CHAR_LUKIFER 0
#define CHAR_REDMOND 1
#define CHAR_GANDREY 2
#define CHAR_HAROLD  3

#define COL_DRAW_FLASH 0
#define COL_DRAW_SHAKE 1

#define COL_MISC_OBJECTION 0
#define COL_MISC_WAIT 1
#define COL_MISC_HOWSTHIS 2
#define COL_MISC_NOCHANGE 3

#define COL_FLAG_NEXTSTEP 1
#define COL_FLAG_TYPING 1 << 1

#define COL_FRONT_DIALOGUE 0

struct {
	uint8_t charIndex, charPose, charSide, flags;
	char *string;
} COL_Court;

void signalMisc(uint8_t index) {
	switch (index)
	{
		case COL_MISC_OBJECTION:

			break;
		case COL_MISC_WAIT:

			break;
		case COL_MISC_HOWSTHIS:
			
			break;
		case COL_MISC_NOCHANGE:

			break;
	}
};

void signalFrontend(uint8_t index); // frontend-specific, may not even be used

void changeDialogue(uint8_t character, uint8_t pose, uint8_t side, char *string) {
	COL_Court.charIndex = character;
	COL_Court.charPose = pose;
	COL_Court.charSide = side;
	COL_Court.string = string;
	COL_Court.flags = COL_FLAG_TYPING;
	signalFrontend(COL_FRONT_DIALOGUE);
};

uint16_t move;

#include "case.h"

uint8_t input;
uint8_t moveTimer;

void start() {
	caseStep();
};

void step() {
	if ((input & 1) && !(COL_Court.flags & COL_FLAG_TYPING) || (COL_Court.flags & COL_FLAG_NEXTSTEP) && !(COL_Court.flags & COL_FLAG_TYPING) && moveTimer == 0) {
		COL_Court.flags = 0;
		caseStep();
		move++;
	};
};
