// A tool for reading and converting .txt files of COL cases into C code that can be put in a header file to embed directly into a build of the game.

#include <stdio.h>
#include <stdint.h>

void printHelp(char *progName[]) {
	puts("Court of Loons: Case to Code");
	printf("Usage: ", progName, " {-h -nmyCase} [CASE.txt]\n");
	puts("\nArguments:");
	puts("-h : Print this help and exit");
	puts("-nX : Set name of the case. Default: case");
};

#ifdef DEBUGPRINT
#define Dprint(str) puts(str)
#else
#define Dprint(str) {}
#endif

uint8_t main(int argc, char *argv[]) {
	FILE *colcase;
	if (argc > 1)
		colcase = fopen(argv[1], "rw");
	else
		colcase = fopen("case.txt", "rw");
	Dprint("Reading case.txt...");
        if (colcase == NULL) {
		puts("Failed to open case.txt!");
		return 1;
	};
	
	char read;
	Dprint("Beginning file reading");

	puts("void caseStep() {\n\tswitch (move) {");
	uint16_t move = 0;
	uint16_t prevMove = -1;
	while (1) {
		fread(&read, 1, 1, colcase);
		Dprint("Read byte");
		if (read == 0) {
			Dprint("read = 0, breaking");
			break;
		};
		if (feof(colcase)) {
			Dprint("EOF, breaking");
			break; // end of file
		};
		if (read == '\n') {
			continue; // nothing's here, who bother?
		};
		if (read == '#') {
			Dprint("Found comment, skipping");
			while (read != '\n') {
				fread(&read, 1, 1, colcase);
			};
			continue; // halt here and loop again
		};
		if (move != prevMove) printf("\t\tcase %i:\n\t\t\t", move);
		else printf("\t\t\t");
		if (read == '>') {
			Dprint("Found special event, parsing");
			fread(&read, 1, 1, colcase); // read the next character
			switch (read)
			{
				case '0': puts("signalDraw(COL_DRAW_FLASH);"); break;
				case '1': puts("signalDraw(COL_DRAW_SHAKE);"); break;
				case '2': puts("signalMisc(COL_MISC_OBJECTION);"); break;
				case '3': puts("signalMisc(COL_MISC_WAIT);"); break;
				case '4': puts("signalMisc(COL_MISC_HOWSTHIS);"); break;
				case 'X': puts("signalMisc(COL_MISC_NOCHANGE);"); break;
			}
			prevMove = move;
			continue;
		};
		if (read == '!') {
			Dprint("Found sound effect, parsing");
			fread(&read, 1, 1, colcase);
			printf("playSound(%c", read);
			puts(");");
			prevMove = move;
			continue;
		};
		if (read == '@') {
			Dprint("Found music, parsing");
			fread(&read, 1, 1, colcase);
			if (read == 'X')
				printf("stopMusic(");
			else
				printf("playMusic(%c", read);
			puts(");");
			prevMove = move;
			continue;
		};
		// here on out, we assume that dialogue is here
		Dprint("Parsing supposed character string...");
		printf("changeDialogue(");
		switch (read)
		{
			case 'L': default:
				printf("CHAR_LUKIFER, ");
				Dprint("Char is Lukifer");
				break;
			case 'R':
				printf("CHAR_REDMOND, ");
				Dprint("Char is Redmond");
				break;
			case 'G':
				printf("CHAR_GANDREY, ");
				Dprint("Char is Gandrey");
				break;
			case 'H':
				printf("CHAR_HAROLD, ");
				Dprint("Char is Harold");
				break;
		};
		fread(&read, 1, 1, colcase);
		Dprint("Read byte");
		printf("%c, ", read, ", ");
		fread(&read, 1, 1, colcase);
		Dprint("Read byte");
		printf("%c, ", read, ", ");
		printf("\"");
		fseek(colcase, 1, SEEK_CUR);
		while (read != '\n') {
			fread(&read, 1, 1, colcase);
			if (read == '\n') break;
			if (read == '\'' || read == '\"') printf("\\");
			printf("%c", read);
		};
		puts("\");\n\t\t\tbreak;");
		prevMove = move;
		move++;
	};
	puts("\t}\n};");

	return 0;
}
