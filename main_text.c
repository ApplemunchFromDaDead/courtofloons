// Text interface for Court of Loons

#include <stdio.h>
#include <stdint.h>
#ifdef __LINUX__
#include <time.h>
#endif

#include "game.h"

char *lukiferPoses[9] = {
	"stands tall",
	"looks content",
	"slams his desk in rage",
	"gives a feral cat's stare",
	"recoils surprisedly",
	"slumps in shame",
	"points with conviction",
	"stances with a vindiction",
	"cowers in confusion"
};

char *redmondPoses[8] = {
	"stands comfortably",
	"smugs up like the vixen she is",
	"leans with her hands on the desk",
	"gives a startling look",
	"bounces back with alimony",
	"hides her face in her hands",
	"crosses her arms below her chest",
	"cross-eyes her percieved threat"
};

char *gandreyPoses[8] = {
	"stands above all idley",
	"delightfully grins",
	"beats down with his gavel",
	"rises with disdain",
	"rises with a fearful tremor",
	"exasperates accordingly",
	"counters with the contempt of court",
	"fringes for lack of respect"
};

char *haroldPoses[8] = {
	"stands cluelessly",
	"gains a cocky smile",
	"bares his teeth with primal anger",
	"wide-eyes almost immediately",
	"cowers like an animal",
	"manages to look somewhat normal",
	"shushes the offending thing", // Hideous Attorney?
	"beams like a laser"
};

void playSound(uint8_t index) {};
void playMusic(uint8_t index) {};
void stopMusic() {};
void signalDraw(uint8_t index) {};

void signalFrontend(uint8_t index) {
	if (index == COL_FRONT_DIALOGUE) {
		char **poseStrings;
		switch (COL_Court.charIndex)
		{
			case CHAR_LUKIFER: default: printf("[Lukifer "); poseStrings = &lukiferPoses; break;
			case CHAR_REDMOND: printf("[Redmond "); poseStrings = &redmondPoses; break;
			case CHAR_GANDREY: printf("[Gandrey "); poseStrings = &gandreyPoses; break;
			case CHAR_HAROLD: printf("[Harold "); poseStrings = &haroldPoses; break;
		};
		printf("%s]\n", poseStrings[COL_Court.charPose]);
		puts(COL_Court.string);
		COL_Court.flags = 0; // no need to initialize typing, it's outputted immediately
	};
};

uint8_t main() {
	char charInput = 0;
	start();
	while (charInput != 'q') {
		input = 1;
		step();
		scanf("%c", &charInput);
	};
};
