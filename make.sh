# !/bin/sh
# Make script for Court of Loons
# Licensed under CC0

COMPILER="gcc"

if [ ! $1 ]; then
	echo "Please give an argument for the target frontend. (text, sdl, etc)"
	exit 1
fi

if [ $1 = "text" ]; then
	$COMPILER main_text.c $2 -o CourtofLoonsTXT
elif [ $1 = "sdl" ]; then
	$COMPILER main_sdl.c -lSDL2 -lSDL2_image -lSDL2_mixer $2 -o CourtofLoons
elif [ $1 = "c2c" ]; then
	$COMPILER case2code.c $2 -o case2code
elif [ $1 = "love" ]; then # lol
	echo "Redmond is sexy and sweet, I get it."
fi
