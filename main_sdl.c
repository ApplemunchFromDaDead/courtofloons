// SDl2 frontend for Court of Loons

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>

#include "game.h"

#define WIDTH 640
#define HEIGHT 400

static const uint8_t fontOffsets_left[90] = {
        25, 16, 8, 13, 10, 17, 25, 23, 23, 8, 9, 23, 3, 22, 14,
        11, 13, 10, 14, 13, 13, 11, 13, 16, 10, 24, 22, 9, 8, 9, 10, 8,
        3, 2, 4, 4, 6, 2, 2, 6, 4, 6, 6, 9, 10, 3, 9,
        8, 9, 6, 9, 8, 8, 8, 6, 12, 7, 8, 10, 2, 6, 6, 7, 6, 8, 8, 7, 6, 7, 2, 7, 8, 7,
        1, 3, 2, 4, 2, 4, 3, 0, 3, 1, 6, 5, 1, 4, 4, 5, 4
};

Mix_Music *music[3];
Mix_Chunk *sfx[8];

Mix_Chunk *sfxSDL[2];

SDL_Texture *wintext;

SDL_Texture *sprites[34];
SDL_Texture *charsprites[32];

SDL_Texture *font;

SDL_Rect dspdest_rect;
SDL_Rect fade_rect;

SDL_Point zeropoint = {0,0};

SDL_Renderer *render;
SDL_Window *win;

uint8_t *keystates;

uint8_t typeTime;
uint8_t typeNum;

uint8_t running;

void playSound(uint8_t index) {
	Mix_PlayChannel(-1, sfx[index], 0);
};

void playMusic(uint8_t index) {
	Mix_PlayMusic(music[index], -1);
};

void stopMusic() {
	Mix_PauseMusic();
};

void signalDraw() {};

void signalFrontend(uint8_t index) {
	if (index == COL_FRONT_DIALOGUE) {
		COL_Court.flags = COL_FLAG_TYPING;
		typeNum = 0;
	};
};

void drawSprite(SDL_Texture *sprite, int x, int y, uint8_t flipped) {
        dspdest_rect = (SDL_Rect){x, y, 0, 0};
        SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
        SDL_RenderCopyEx(render, sprite, &dspdest_rect, &dspdest_rect, 0, &zeropoint, flipped ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE);
};

void drawRepeatingSprite(SDL_Texture *sprite, int x, int y) {
        int _x = x;
        if (x == y) {
                dspdest_rect.x = dspdest_rect.y = x;
        } else {
                dspdest_rect.x = x;
                dspdest_rect.y = y;
        }
        SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
        uint8_t limit = 0;
        for (; _x < WIDTH && limit <= 30; limit++) {
                if (_x < (-WIDTH << 2)) {
                        _x += dspdest_rect.w;
                        continue;
                };
                SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
                _x += dspdest_rect.w;
                dspdest_rect.x = _x;
        };
        limit = 0;
        _x = x;
        for (; _x > -dspdest_rect.w && limit <= 30; limit++) {
                SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
                _x -= dspdest_rect.w;
                dspdest_rect.x = _x;
        };
};

void drawSpriteSheeted(SDL_Texture *sprite, int x, int y, int frame, int x_sprdist, int y_sprdist, uint8_t flipped) { // supports only purely horizontal sheets
        if (x < -x_sprdist || x > x_sprdist + WIDTH) return;
        SDL_Rect destrect = {x, y, x_sprdist, y_sprdist};
        dspdest_rect.x = x_sprdist * frame;
        dspdest_rect.y = 0; // this somehow fixes an issue where the sprite would vertically stretch when moving the camera vertically, but crop off within the sprite's intended bounds. what?
        dspdest_rect.w = x_sprdist,
        dspdest_rect.h = y_sprdist,
        SDL_RenderCopyEx(render, sprite, &dspdest_rect, &destrect, 0, &zeropoint, flipped ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE);
};

void drawTextString(const char *stuff, int x, int y, uint8_t typenum) {
	// TOO-DO: add soft word wrapping, change to monospace font
        uint16_t _x = x;
	uint16_t _y = y;

        for (uint8_t i = 0; i < (typenum == 0) ? 255 : typenum; i++) {
        	if (stuff[i] == '\0') break; // terminator character? then get outta there before stuff gets nasty
        	if (_x < -30) return;
		//if (_y / 32 > 2 && i < _y / 32) continue;
		if (_y - y >= 96) i += 24 * (_y - y / 30);
		if (_x + 30 > WIDTH) {
			_x = x;
			_y += 32;
		};
		if (typenum != 0 && i > typenum) return;
        	SDL_Rect destrect = {_x, _y, 30, 30};
        	dspdest_rect.x = 30 * (stuff[i] - 33);
        	dspdest_rect.y = 0;
        	dspdest_rect.w = dspdest_rect.h = 30;
        	SDL_RenderCopy(render, font, &dspdest_rect, &destrect);
        	_x += (stuff[i] == 32) ? 30 : 30 - fontOffsets_left[stuff[i] - 33];
        };
};

void draw() {
	drawSprite(sprites[0], 0, 0, COL_Court.charSide);
	drawSprite(charsprites[COL_Court.charIndex * 8 + COL_Court.charPose], 0, 0, COL_Court.charSide);
	drawSprite(sprites[1], 0, 0, COL_Court.charSide);
	drawTextString(COL_Court.string, 0, 280, typeNum);
};

uint8_t main() {
        if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_EVENTS |
 MIX_INIT_MID | SDL_INIT_JOYSTICK) != 0) return 1;
        if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 1, 2048) != 0)
                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_WARNING, "No audio!", "The game couldn't find a usable audio system. Check your audio devices!", NULL);

	win = SDL_CreateWindow("Court of Loons (SDL)", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	render = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	SDL_RenderSetLogicalSize(render, WIDTH, HEIGHT); // keep that cwispy 270P 16:9 wesowutiown UwU
	
	keystates = SDL_GetKeyboardState(NULL);

	font = IMG_LoadTexture(render, "sprites/fontmap.png");

	sfx[0] = Mix_LoadWAV("sfx/slam.wav");
	sfx[1] = Mix_LoadWAV("sfx/awkward.wav");
	sfx[2] = Mix_LoadWAV("sfx/slam.wav");
	sfx[5] = Mix_LoadWAV("sfx/beep.wav");

	sfxSDL[0] = Mix_LoadWAV("sfx/blip.wav");

	music[0] = Mix_LoadMUS("music/semantics.mid");
	music[1] = Mix_LoadMUS("music/mouse.mid");
	music[2] = Mix_LoadMUS("music/degeneration.mid");

	sprites[0] = IMG_LoadTexture(render, "sprites/bgside.png");
	sprites[1] = IMG_LoadTexture(render, "sprites/bgsideDESK.png");

	charsprites[0] = IMG_LoadTexture(render, "sprites/lukifer/normalI.png");
	charsprites[1] = IMG_LoadTexture(render, "sprites/lukifer/smugI.png");
	charsprites[2] = IMG_LoadTexture(render, "sprites/lukifer/frustratedI.png");
	charsprites[3] = IMG_LoadTexture(render, "sprites/lukifer/exasperatedI.png");
	charsprites[4] = IMG_LoadTexture(render, "sprites/lukifer/shockI.png");
	charsprites[5] = IMG_LoadTexture(render, "sprites/lukifer/normalI.png");
	charsprites[6] = IMG_LoadTexture(render, "sprites/lukifer/howsthisI.png");
	charsprites[7] = IMG_LoadTexture(render, "sprites/lukifer/angryI.png");

	charsprites[8] = IMG_LoadTexture(render, "sprites/redmond/normalI.png");
	charsprites[9] = IMG_LoadTexture(render, "sprites/redmond/smugI.png");
	charsprites[10] = IMG_LoadTexture(render, "sprites/redmond/frustratedI.png");
	charsprites[11] = IMG_LoadTexture(render, "sprites/redmond/normalI.png");
	charsprites[12] = IMG_LoadTexture(render, "sprites/redmond/normalI.png");
	charsprites[13] = IMG_LoadTexture(render, "sprites/redmond/normalI.png");
	charsprites[14] = IMG_LoadTexture(render, "sprites/redmond/normalI.png");
	charsprites[15] = IMG_LoadTexture(render, "sprites/redmond/normalI.png");
	
	running = 1;
	SDL_Event event;
	start();
	typeTime = 0;
	typeNum = 0;
	while (running) {
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
				case SDL_QUIT: running = 0; break;
			}
		};
		if (keystates[SDL_SCANCODE_Z]) input = 1;
		step();
		input = 0;
		draw();

		if (typeTime > 0) typeTime--;
		if (typeTime == 0 && (COL_Court.flags & COL_FLAG_TYPING)) {
			typeTime = 3;
			typeNum++;
			if (typeNum % 2) Mix_PlayChannel(-1, sfxSDL[0], 0);
			if (COL_Court.string[typeNum + 1] == 0) COL_Court.flags ^= COL_FLAG_TYPING;
		};

                SDL_RenderPresent(render);
                SDL_RenderClear(render);
                SDL_Delay(16.667f);

	};
};
